<?php

class LinkRemoveHTMLSimpleTest extends UnitTestCase {


 	private $basicArgs = array("filter_search_content" => "0",
 							   "filter_search_excerpt" => "0",
		 					   "filter_search_title" => "0",
		 					   "filter_allow_images" => "0",
		 					   "filter_allow_styles" => "0",
		 					   "filter_allow_links" => "0"
		 
 							   );
	private $post = array("post_content" => "K<a href='http://www.bla.com/bla'>Blablabla</a>K http://www.roguelink", 
						  "post_title" => "Emtpy",
						  "post_excerpt" => "<a href='http://www.bla.com/bla'>Blablabla</a>"); 
	
	protected static function getMethod($name) {
	  $class = new ReflectionClass('faf_remove_html');
	  $method = $class->getMethod($name);
	  $method->setAccessible(true);
	  return $method;
 	}
 	
 	public function testDefaultReturnNoChanges() {
 	     $f = new faf_remove_html($this->post,$this->basicArgs); 
        $p = $f->execute();
        $this->assertEqual($this->post, $p);
 	
 	}

	public function testRemoveStyles() { 
	
	
	}
	public function testRemoveImage() {
	
	
	}
	public function testRemoveLinks() {
		
		$content["post_content"] = "Content http://www.roguelink.com <a href='http://www.normallink.com'>link</a> Content"; 
		$content_expected["post_content"] = "Content Content"; 
		
		$args["filter_search_content"] = 1; 
			
		
		$f = new faf_link_filter($content,$args); 
        $p = $f->execute();
	    
	    $this->assertEqual($this->post, $content_expected);
	
	} 	
}
?>
