<?php

class CategoryFilterSimpleTest extends FafTester {


	/* 
	Unit test private functions
	*/
	protected static function getMethod($name) {
	  $class = new ReflectionClass('faf_add_keyword_category');
	  $method = $class->getMethod($name);
	  $method->setAccessible(true);
	  return $method;
	}

	function testExecute()
	{
	
		$post = array("post_content" => "<p>Test Content</p> <img src='http://no'> <br /> bla bla for crap", 
					 "post_excerpt" => "test Content something else", 
					 "post_title" => "Keyword test big title "
					 ); 
					 
		$args = array("filter_search_title" => 0, 
					"filter_search_content" => 1, 
					"filter_search_excerpt" => 0, 
					"filter_match_case" => 0, 
					"filter_match_word" => 0, 
					"filter_value" => "",	 
					"faf_filter_categories" => array(45) ); 
		
		$method = $this->getMethod("execute"); 
				
		// Match keyword to certain category on content 
		$args["filter_value"] = "bla"; 
		$obj = new faf_add_keyword_category($post,$args); 
  		$result = $method->invokeArgs($obj, array());	
  		 $this->assertEqual($result["tax_input"]["category"],$args["faf_filter_categories"]); 
		
		// Match keyword to cat on title 
		$args["filter_search_title"] = 1; 
		$args["filter_search_content"] = 0; 
		$args["filter_search_excerpt"] = 0; 
		$args["filter_value"] = "title"; 

		$obj = new faf_add_keyword_category($post,$args); 
  		$result = $method->invokeArgs($obj, array());	
  		 $this->assertEqual($result["tax_input"]["category"],$args["faf_filter_categories"]); 
		

		// Match keyword to cat on excerpt
		$args["filter_search_title"] = 0; 
		$args["filter_search_content"] = 0; 
		$args["filter_search_excerpt"] = 1; 
		$args["filter_value"] = "something"; 

		$obj = new faf_add_keyword_category($post,$args); 
  		$result = $method->invokeArgs($obj, array());	
  		$this->assertEqual($result["tax_input"]["category"],$args["faf_filter_categories"]); 

/** Next few match on content */
		$args["filter_search_title"] = 0; 
		$args["filter_search_content"] = 1; 
		$args["filter_search_excerpt"] = 0; 

	
		// Match full word
		$args["filter_match_word"] = 1; 
		$args["filter_value"] = "content"; 
		
		$obj = new faf_add_keyword_category($post,$args); 
  		$result = $method->invokeArgs($obj, array());	
  		$this->assertKeyExists($result,"tax_input,category");
  		
  		$this->assertEqual($result["tax_input"]["category"],$args["faf_filter_categories"]); 
		
		// Fail on full word
		$args["filter_value"] = "con"; 		  
		$obj = new faf_add_keyword_category($post,$args); 
  		$result = $method->invokeArgs($obj, array());	
  		$this->assertKeyExists($result,"tax_input,category");
  		$this->assertKeyNotExists($result,"tax_input,category" ); 

  					
		// Match case
		$args["filter_match_word"] = 0; 
		$args["filter_match_case"] = 1;
		$args["filter_value"] = "Content"; 
		
		$obj = new faf_add_keyword_category($post,$args); 
  		$result = $method->invokeArgs($obj, array());	

  		$this->assertEqual($result["tax_input"]["category"],$args["faf_filter_categories"]); 

		// Fail on case mismatch
		$args["filter_value"] = "content"; 		  
		$obj = new faf_add_keyword_category($post,$args); 
  		$result = $method->invokeArgs($obj, array());	
  		$this->assertEqual($result["tax_input"]["category"],array() ); 
  		
  				
		// Match comma-seperated
		$args["filter_match_word"] = 0; 
		$args["filter_match_case"] = 0;

		// two keywords match		
		$args["filter_value"] = "content,crap"; 		  
		$obj = new faf_add_keyword_category($post,$args); 
  		$result = $method->invokeArgs($obj, array());	
    	$this->assertEqual($result["tax_input"]["category"],$args["faf_filter_categories"]); 

		// one match
		$args["filter_value"] = "content,-,bulshirt"; 		  
		$obj = new faf_add_keyword_category($post,$args); 
  		$result = $method->invokeArgs($obj, array());	
    	$this->assertEqual($result["tax_input"]["category"],$args["faf_filter_categories"]); 

		// none match
		$args["filter_value"] = "bulshirt,-"; 		  
		$obj = new faf_add_keyword_category($post,$args); 
  		$result = $method->invokeArgs($obj, array());	
    	$this->assertEqual($result["tax_input"]["category"],array()); 
  			
		// Match with spaces (multiple)
		$args["filter_value"] = " content, crap "; 		  
		$obj = new faf_add_keyword_category($post,$args); 
  		$result = $method->invokeArgs($obj, array());	
    	$this->assertEqual($result["tax_input"]["category"],$args["faf_filter_categories"]);
    			
		// Match multiple categories
		$args["faf_filter_categories"] = array(45,46); 
		
		$args["filter_value"] = " content, crap "; 		  
		$obj = new faf_add_keyword_category($post,$args); 
  		$result = $method->invokeArgs($obj, array());	
    	$this->assertEqual($result["tax_input"]["category"],$args["faf_filter_categories"]);	

		// No match 
		$args["filter_value"] = "nai"; 		  
		$obj = new faf_add_keyword_category($post,$args); 
  		$result = $method->invokeArgs($obj, array());	
    	$this->assertEqual($result["tax_input"]["category"],array());	
	}

}
?>
