��          �   %   �      0  
   1  
   <     G  #   M     q     �  '   �  I   �  L   !     n     �  
   �  <   �  =   �  >        \  5   l  a   �       y        �     �     �  �  �     2  
   B     M  .   R  '   �  "   �  ,   �  G   �  Q   A     �     �     �  K   �  Q   	  F   o	     �	  1   �	  g   �	  &   `
  �   �
                                                                                     	                                                 
        Add Filter Currently: Error Execute filters for all feeds first Execute per feed filters first Feedwordpress Advanced Filters Feedwordpress Advanced Filters Overview Feedwordpress Advanced Filters requires at least PHP version 5.3 to work! Feedwordpress plugin is required for Feedwordpress Advanced Filters to work! Filter processing settings Image filters New Filter Only excute filters for all feeds (ignore per feed settings) Only execute filters per feed (ignore filters for all feeds)  Please remember to click the Save button after making changes! Search in Title Some fields are required. Please fill them out first! This page will display the filters from all feeds in the order they will be executed on the posts Use the site-wide setting You can pick any number of Filters. The meaning of the values of every filter can differ. Some have extra options as well by change version Project-Id-Version: Feedwordpress Advanced Filters
Report-Msgid-Bugs-To: http://wordpress.org/tag/faf
POT-Creation-Date: 2013-06-27 12:32:55+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2014-12-07 09:44+0300
Last-Translator: 
Language-Team: Bas Schuiling <bas@weblogmechanic.com>
X-Generator: Poedit 1.5.4
Language: Dutch
 Voeg filter toe Momenteel: Fout Voer de filters voor alle feeds als eerste uit Voer de filters per feed als eerste uit Feedwordpress geavanceerde filters Feedwordpress geavanceerde filters overzicht Feedwordpress geavanceerde filters heeft minimaal PHP versie 5.3 nodig! Feedwordpress is vereist voor het gebruik van Feedwordpress geavanceerde filters! Verwerkingsinstellingen filter Afbeeldingsfilter Nieuw filter Voer alleen filters uit van alle feeds ( negeer de instellingen per feed )  Voer alleen de filters per feed uit ( negeer de globale filters voor alle feeds ) Vergeet niet op de bewaarknop te drukken na het maken van wijzigingen! In titel zoeken Sommige velden zijn verplicht. Vul deze eerst in. Deze pagina toont alle filters van alle feeds in de volgorde zoals ze worden uitgevoerd op de berichten Gebruik instelling voor gehele website U kunt elke combinatie van filters gebruiken. De betekenis van instellingen kan per filter verschillen. Sommigen hebben ook extra opties door verander versie 