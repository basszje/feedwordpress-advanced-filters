<?php
error_reporting(E_ALL);

//console("KUT");

require_once( WP_CONTENT_DIR . '/plugins/toppa-plugin-libraries-for-wordpress/ToppaFunctionsFacadeWp.php');
Mock::generate('ToppaFunctionsFacadeWp');

class FafTester extends UnitTestCase
{
   function FafTester($name = false) {
        $this->UnitTestCase($name);
    }

  function assertKeyNotExists($array,$arrkey, $message = "%s")
  {
  	$keys = explode(",",$arrkey); 
  	$dim = "";
  	foreach($keys as $k) 
  	{	$dim .= "[$k]"; }
  	
  	echo $dim;
  	$this->assertFalse( isset( ${$array . $dim} )) ;
  		 
  }

  function assertKeyExists($array,$arrkey, $message = "%s")
  {
    	$keys = explode(",",$arrkey); 
  	$dim = "";
  	foreach($keys as $k) 
  		$dim .= "[$k]"; 
  		
  	echo $dim;

  	$this->assertTrue( isset( ${$array[$dim]} ) ) ;
  		 
  }

 function arrayWalk($array,$key)
 {
 	return 0 < count(
    array_filter(
        $array,
        function ($key) {
            return array_key_exists($key,$array);
        }
   	 )
   );
}

}


// Start tests 
	
	$tests = array(); 
//	echo "IMAGE TESTING IS OFF!"; 
	$tests[] = array("name" => "Image Filters", "path" => "/tests/unit_image_filter.php", "passes" => "y"); 
 	$tests[] = array("name" => "Remove Keyword", "path" => "/tests/remove_keyword.php", "passes" => "y");  
 	$tests[] = array("name" => "Add Keyword to Category", "path" => "/tests/unit_category_keyword.php", "passes" => "y");  
 	$tests[] = array("name" => "Link Filter", "path" => "/tests/unit_link_filter.php", "passes" => "y");  
 	$tests[] = array("name" => "HTML Filter", "path" => "/tests/unit_remove_html_filter.php", "passes" => "y");  
 	
	foreach($tests as $short) 
	{
	$test = new TestSuite($short["name"]); 
 	$reporter = new WpSimpleTestReporter($short['passes']);
 	$path = plugin_dir_path(__FILE__) . $short["path"];
        $test->addFile($path);
        $test->run($reporter);          
	}




echo "<style>.SimpleTest {
    white-space: nowrap;
    background: none repeat scroll 0 0 #ffffff;
    border: 1px solid #eeeeee;
    font-family: 'Courier New',Courier,monospace;
    margin-bottom: 22px;
    overflow: auto;
    padding: 11px;
}
.SimpleTestHeader {}

.SimpleTestSummary {
    padding: 8px;
    margin-top: 1em;
    color: white;
}

.SimpleTestFailBarColor {
    background-color: red;
}

.SimpleTestPassBarColor {
    background-color: green;
}

.fail {
    color: red;
}

.pass {
    color: green;
} </style>";
?>
